package com.syl.generator.common.dao;

import com.syl.generator.common.bean.DatabaseBean;
import com.syl.generator.common.bean.SystemConfigBean;
import com.syl.generator.view.bean.GeneratorConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author syl
 * @create 2018-03-23 15:21
 **/
public interface SqlLiteMapper {
    // 系统参数配置 sql语句开始
    boolean isExistSystemConfig();

    int saveSystemConfig(SystemConfigBean bean);

    int updateSystemConfig(SystemConfigBean bean);

    SystemConfigBean selectSystemConfig();

    int removeSystemConfig();

    // db连接 sql语句开始
    boolean isExistDbConnection(String name);

    int saveDbConnection(DatabaseBean bean);

    int removeDbConnection(String id);

    int updateDbConnection(DatabaseBean bean);

    DatabaseBean selectOneDbConnection(String id);

    List<DatabaseBean> selectAllDbConnection();

    // 生成模板配置列表 sql语句开始
    boolean isExistGeneratorConfig(String name);

    int saveGeneratorConfig(@Param("name") String name, @Param("json") String json);

    int removeGeneratorConfig(@Param("name") String name);

    int updateGeneratorConfig(@Param("name") String name, @Param("json") String json);

    List<GeneratorConfig> selectGeneratorConfigAll();
}
