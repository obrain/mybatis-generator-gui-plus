package com.syl.generator.common.bean;

import com.syl.generator.common.constant.BaseConstant;
import com.syl.generator.common.enums.DbType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Created by Owen on 5/13/16.
 * 数据库基础bean 数据库类型和基础信息在com.syl.generator.common.enums.DbType中配置
 */
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class DatabaseBean {

	/**
	 * The primary key in the sqlite db
	 */
	private String id;
	/**
	 * 表名称
	 */
	private String tableName;
	/**
	 * 表注释
	 */
	private String tableComment;
	/**
	 * db连接类型
	 */
	private String dbType;

	private String driverClass;

	private String connectionURL;
	/**
	 *
	 */
	private String name;

	private String host;

	private int port;

	/**
	 * 数据库名称
	 */
	private String schema;

	private String username;

	private String password;

	private String encoding;

	public DatabaseBean() {}

	public DatabaseBean(DbType dbType, String host, int port, String schema, String username, String password, String encoding){
		this.dbType = dbType.name();
		DbType type = dbType;
		this.driverClass = type.getDriverClass();
		this.username = username;
		this.password = password;
		this.encoding = encoding;
		this.host = host;
		this.port = port;
		this.schema = schema;

		String pattern = type.getConnectionUrlPattern();
		pattern = pattern.replace(BaseConstant.REPLACE_HOST,this.getHost())
						 .replace(BaseConstant.REPLACE_PORT,this.getPort()+"")
						 .replace(BaseConstant.REPLACE_DATABASE_NAME,this.getSchema())
						 .replace(BaseConstant.REPLACE_ENCODING,this.getEncoding());
		this.connectionURL = pattern;
	}

}
