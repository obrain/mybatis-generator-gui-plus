package com.syl.generator.common.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 输入框验证bean
 *
 * @author syl
 * @create 2018-03-18 1:36
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class InputVerifyBean {
    private boolean pass;
    private String  message;

    public InputVerifyBean(boolean pass, String message) {
        this.pass = pass;
        this.message = message;
    }
}
