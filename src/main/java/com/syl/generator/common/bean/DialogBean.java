package com.syl.generator.common.bean;

import javafx.scene.Parent;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author syl
 * @create 2018-03-23 10:11
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class DialogBean<Controller> {
    private Controller controller;
    private Parent parent;
    private Stage dialogStage;

}
