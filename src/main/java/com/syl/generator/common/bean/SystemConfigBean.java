package com.syl.generator.common.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 系统参数bean
 *
 * @author syl
 * @create 2018-03-23 22:42
 **/

@Setter
@Getter
@ToString
@Accessors(chain = true)
public class SystemConfigBean {
    private String name;
    private String brand;
    private String toolsPath;
    private int    mode;
}
