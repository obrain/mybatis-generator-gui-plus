package com.syl.generator.common.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 数据库表列信息
 * @author syl
 * @create 2018-04-01 21:12
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class TableColumnBean implements Serializable {
    private String configID;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 表注释
     */
    private String tableComment;

    private String originaColumnName;
    /**
     * 列名
     */
    private String columnName;
    /**
     * 列类型
     */
    private String columnType;
    /**
     * 主键
     */
    private String columnKey;
    /**
     * 列注释
     */
    private String columnComment;
    /**
     * 新增属性
     */
    private boolean acquired;
    /**
     * 新增属性符号
     */
    private String  symbol;
}
