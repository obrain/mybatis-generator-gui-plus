package com.syl.generator.common.constant;

/**
 * 基础常量类
 *
 * @author syl
 * @create 2018-03-23 18:38
 **/
public class BaseConstant {
    //connectionUrlPattern
    public static final String REPLACE_HOST = "HOST";
    public static final String REPLACE_PORT = "PORT";
    public static final String REPLACE_DATABASE_NAME = "DATABASE_NAME";
    public static final String REPLACE_ENCODING = "ENCODING";

    //图标区
    public static final String SYSTEM_ICON = "icons/system-config.png";
    public static final String TABLE_ICON = "icons/table.png";
    public static final String COMPUTER_ICON = "icons/computer.png";
    public static final String CONFIG_ICON = "icons/config-list.png";

    //MbgDbInfoMapper 接口反射使用
    public static final String TABLE_METHOD_NAME = "queryXXXTableNames";
    public static final String COLUMN_METHOD_NAME = "queryXXXColumns";

    //生成区
    public static final String DEFLAULT_TEMPLATE_DIRECTORY = "template";
    public static final String FREEMARKER_VERSION = "2.3.0";
    public static final String TIME_FORMAT = "yyyy-MM-dd";

    //配置区
    public static final String TEMPLATE_BEAN_DIRECTORY = "template\\java\\bean";
    public static final String TEMPLATE_CONTROLLER_DIRECTORY = "template\\java\\controller";
    public static final String TEMPLATE_DAO_DIRECTORY = "template\\java\\dao";
    public static final String TEMPLATE_SERVICE_DIRECTORY = "template\\java\\service";
    public static final String TEMPLATE_MAPPER_DIRECTORY  = "template\\xml";
    public static final String QUERY_HELP_NAME = "QueryCondition";
    public static final String CONVERT_JAVA_TYPE = "filed.type.convert."; //yml数据库类型转换为java类型
    public static final String CONVERT_MAPPER_TYPE = "filed.type.convert."; //yml数据库类型转换为mapper类型

    //普通常量
    public static final String SQLLITE_DB_ID = "sqllite_default";
    public static final String DOT = ".";
}
