package com.syl.generator.common.enums;

import static com.syl.generator.common.constant.BaseConstant.DOT;

/**
 * 文件类型
 *
 * @author syl
 * @create 2018-03-26 19:55
 **/
public enum FileType {
    JAVA("java"),
    XML("xml");

    private String name;
    FileType(String name) {
        this.name = name;
    }

    public String getName() {
        return DOT+name;
    }
}
