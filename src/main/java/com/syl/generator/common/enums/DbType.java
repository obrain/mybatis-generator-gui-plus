package com.syl.generator.common.enums;

import lombok.Getter;

/**
 * 数据库主要配置类
 *
 * @author syl
 * @create 2018-03-23 17:50
 **/
public enum DbType {
    MySQL("com.mysql.jdbc.Driver", "jdbc:mysql://HOST:PORT/DATABASE_NAME?useUnicode=true&useSSL=false&characterEncoding=ENCODING", "MySql"),
    Oracle("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@HOST:PORT:DATABASE_NAME", "Oracle"),
    PostgreSQL("org.postgresql.Driver", "jdbc:postgresql://HOST:PORT/DATABASE_NAME", "PostgreSql"),
    SQL_Server("net.sourceforge.jtds.jdbc.Driver", "jdbc:jtds:sqlserver://HOST:PORT/DATABASE_NAME", "SqlServer"),
    SqlLite("org.sqlite.JDBC","jdbc:sqlite:db/config.db", "SqlLite");

    @Getter
    private String driverClass;
    @Getter
    private String connectionUrlPattern;
    /**
     * dbinfo 接口中反射时使用的名称
     */
    @Getter
    private String daoName;

    DbType(String driverClass, String connectionUrlPattern, String daoName) {
        this.driverClass = driverClass;
        this.connectionUrlPattern = connectionUrlPattern;
        this.daoName = daoName;
    }

}
