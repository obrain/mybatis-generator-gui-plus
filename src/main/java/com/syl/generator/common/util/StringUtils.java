package com.syl.generator.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author syl
 * @create 2018-03-18 2:02
 **/
public class StringUtils {
    public static boolean isEmpty(String str){
        return str == null || str.isEmpty();
    }

    public static boolean isEmptys(String... strs) {
        for (String s:strs){
            if(isEmpty(s))return true;
        }
        return false;
    }

    public static String extract(String regex,String string){
        Pattern r = Pattern.compile(regex);
        Matcher m = r.matcher(string);
        StringBuffer sb = new StringBuffer();
        while (m.find()){
            sb.append(m.group());
        }
        return sb.toString();
    }

    /**
     *
     * convert string from slash style to camel style, such as ab_cd  or ab-cd will convert to AbCd
     *
     * @param str 要转换的字符
     * @param initial 是否首字母大写
     * @return
     */
    public static String underlineToCamel(String str,boolean initial) {
        if (str != null) {
            StringBuilder sb = new StringBuilder();
            if(initial)
                sb.append(String.valueOf(str.charAt(0)).toUpperCase());
            else
                sb.append(String.valueOf(str.charAt(0)).toLowerCase());
            char[] param = str.toCharArray();
            for (int i = 1; i < str.length(); i++) {
                char c = str.charAt(i);
                if ('_' == c) {
                    if (++i < str.length()) {
                        sb.append(Character.toUpperCase(param[i]));
                    }
                } else {
                    sb.append(c);
                }
            }
            return sb.toString();
        }
        return null;
    }
}
