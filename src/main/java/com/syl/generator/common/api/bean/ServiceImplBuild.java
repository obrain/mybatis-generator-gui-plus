package com.syl.generator.common.api.bean;

import com.syl.generator.common.bean.SystemConfigBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author syl
 * @create 2018-04-07 16:53
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class ServiceImplBuild  extends JavaBuild {
    private static final String SERVICE = "ServiceImpl";

    private String serviceName;
    private String beanName;
    private String dtoName;
    private String daoName;

    public ServiceImplBuild(String templateDirectory, String templateFileName, String path, String fileName, String reference, SystemConfigBean systemConfigBean) {
        super(templateDirectory, templateFileName, path, fileName, reference, systemConfigBean);
    }
}
