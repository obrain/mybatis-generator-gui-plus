package com.syl.generator.common.api.bean;

import com.syl.generator.common.bean.SystemConfigBean;
import com.syl.generator.common.enums.FileType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.syl.generator.common.constant.BaseConstant.DOT;

/**
 * @author syl
 * @create 2018-04-04 13:14
 **/
@Accessors(chain = true)
public class JavaBuild extends BaseBuild {
    private List<String> imports = new ArrayList<>();
    private List<String> annotations = new ArrayList<>();
    private StringBuffer jextends = new StringBuffer();
    private StringBuffer jimplements = new StringBuffer();
    @Setter
    @Getter
    private String referencePackage;


    public JavaBuild(String templateDirectory, String templateFileName, String path, String fileName, String reference, SystemConfigBean systemConfigBean) {
        super(templateDirectory,templateFileName,path,fileName, FileType.JAVA);
        this.referencePackage = reference;
        this.setAuthor(systemConfigBean.getName());
        this.setBrand(systemConfigBean.getBrand());
        this.setPath(path + reference.replace(DOT, File.separator)+File.separator);
    }

    public BaseBuild addImport(String $import) {
        if(this.imports.contains($import))return this;
        this.imports.add($import);
        return this;
    }

    public BaseBuild addAnnotation(String annotation) {
        this.annotations.add(annotation);
        return this;
    }

    public String getJextends() {
        return jextends.length() > 0 ? jextends.insert(0,"extends ").toString() : "";
    }

    public JavaBuild setJextends(String $extend) {
        this.jextends = jextends;
        return this;
    }

    public String getJimplements() {
        return jimplements.length() > 0 ? jimplements.insert(0,"implements ").substring(0,jimplements.length()-1).toString() : "";
    }

    public JavaBuild addJimplements(String implement) {
        this.jimplements.append(implement).append(",");
        return this;
    }

    public List<String> getImports() {
        imports = imports.stream().sorted(Comparator.comparing(String::length)).collect(Collectors.toList());
        System.out.println(imports);
        return imports;
    }

    public List<String> getAnnotations() {
        annotations = annotations.stream().sorted(Comparator.comparing(String::length)).collect(Collectors.toList());
        return annotations;
    }

    public String getReference(){
        return referencePackage +DOT+getFileName();
    }


}
