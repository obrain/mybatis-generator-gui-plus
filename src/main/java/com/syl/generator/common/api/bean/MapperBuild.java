package com.syl.generator.common.api.bean;

import com.syl.generator.common.bean.SystemConfigBean;
import com.syl.generator.common.bean.TableColumnBean;
import com.syl.generator.common.enums.DbType;
import com.syl.generator.common.enums.FileType;
import com.syl.generator.common.util.ConfigReadUtil;
import com.syl.generator.common.util.StringUtils;
import com.syl.generator.view.bean.GeneratorConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

import static com.syl.generator.common.constant.BaseConstant.CONVERT_MAPPER_TYPE;
import static com.syl.generator.common.constant.BaseConstant.DOT;
import static com.syl.generator.common.constant.BaseConstant.TEMPLATE_MAPPER_DIRECTORY;

/**
 * @author syl
 * @create 2018-04-05 20:12
 **/
@Setter
@Getter
@Accessors(chain = true)
public class MapperBuild extends XmlBuild{
    private String REMARK1 = "这是用于编写业务相关的Mapper,该文件可以随便修改";
    private String REMARK2 = "这是生成工具生成的Mapper,注意不要在这里写任何代码(该文件每次生成都会覆盖)";
    private List<Filed> originalFileds = new ArrayList<>();
    private List<Filed> moreFileds = new ArrayList<>();
    private String namespace;
    private String schema;
    private String beanReference;
    private String tableName;
    private String tableComment;
    private String beanName;
    private String beanNameLower;
    private boolean simple;
    private SystemConfigBean systemConfig;

    public MapperBuild(String path, GeneratorConfig config, DbType type,boolean simple) {
        this(path,config,type,null,simple);
    }

    /**
     *
     * @param path
     * @param config
     * @param type
     * @param schema 数据库名称
     * @param simple 是否简单生成mapper
     */
    public MapperBuild(String path, GeneratorConfig config, DbType type,String schema,boolean simple) {
        super(TEMPLATE_MAPPER_DIRECTORY,
                ConfigReadUtil.getYmlString("generate.template.mapper").replace("${DbType}",type.getDaoName()) ,
                path, config.getDomainObjectName()+(simple ? "" : "Generator")+config.getMapperName(), FileType.XML);
        this.beanName = config.getDomainObjectName();
        this.tableComment = config.getTableComment();
        this.beanNameLower = this.beanName.substring(0,1).toLowerCase()+this.beanName.substring(1);
        this.tableName = config.getTableName();
        this.schema = StringUtils.isEmpty(schema) ? "" : Filed.ESCAPES+schema+Filed.ESCAPES+DOT;
        this.simple = simple;
    }

    public MapperBuild addAllFiled(List<TableColumnBean> originalList,List<TableColumnBean> moreList,boolean useOriginalName) {
        for(TableColumnBean bean:originalList){
            String type = StringUtils.extract("[a-z]", bean.getColumnType().toLowerCase());
            String mapperType = ConfigReadUtil.getYmlString(CONVERT_MAPPER_TYPE+type+".mapper", type);
            String key = bean.getColumnKey();
            Filed filed = new Filed(mapperType.toUpperCase(),bean.getColumnName(),useOriginalName).setComment(bean.getColumnComment());
            filed.setPrimaryKey(!StringUtils.isEmpty(key));
            this.addOriginalFiled(filed);
        }
        for(TableColumnBean bean:moreList){
            String type = StringUtils.extract("[a-z]", bean.getColumnType().toLowerCase());
            String key = bean.getColumnKey();
            String columnName = bean.getColumnName();
            Filed filed = new Filed(type.toUpperCase(),columnName,useOriginalName).setComment(bean.getColumnComment());
            if(bean.isAcquired())
                filed.setEscapesName(bean.getOriginaColumnName()).setSymbol(bean.getSymbol()).setAcquired(true);
            filed.setPrimaryKey(!StringUtils.isEmpty(key));
            this.addMoreFileds(filed);
        }
        return this;
    }

    public MapperBuild setSystemConfig(SystemConfigBean systemConfig) {
        this.systemConfig = systemConfig;
        this.setAuthor(systemConfig.getName())
            .setBrand(systemConfig.getBrand())
            .setRemark(simple ? REMARK1 : REMARK2);
        return this;
    }

    public MapperBuild addOriginalFiled(Filed filed){
        this.originalFileds.add(filed);
        return this;
    }

    public MapperBuild addMoreFileds(Filed filed){
        this.moreFileds.add(filed);
        return this;
    }

}
