package com.syl.generator.common.api.bean;

import com.syl.generator.common.constant.BaseConstant;
import com.syl.generator.common.enums.FileType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.syl.generator.common.constant.BaseConstant.DOT;

/**
 *
 * @author syl
 * @create 2018-03-26 19:49
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class BaseBuild {
    private String   templateDirectory;//模板文件目录
    private String   templateFileName;//模板文件名称
    private String   path;
    private String   fileName;
    private FileType fileType;
    private String   author;
    private String   brand;
    private String   date;
    private String   remark;


    public BaseBuild(String templateDirectory,String templateFileName,String path, String fileName, FileType fileType) {
        this.templateDirectory = templateDirectory;
        this.templateFileName = templateFileName;
        this.path = path;
        this.fileName = fileName;
        this.fileType = fileType;
        this.date = new SimpleDateFormat(BaseConstant.TIME_FORMAT).format(new Date());
    }

    /**
     * 生成java 时使用
     * @param path
     * @param fileName
     * @param fileType
     * @param reference
     */
    public BaseBuild(String path, String fileName, FileType fileType,String reference) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.date = new SimpleDateFormat(BaseConstant.TIME_FORMAT).format(new Date());
        this.path = path + reference.replace(DOT, File.separator)+File.separator;
    }


}
