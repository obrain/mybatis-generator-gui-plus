package com.syl.generator.common.api.bean;

import com.syl.generator.common.util.ConfigReadUtil;
import com.syl.generator.common.util.SqlReservedWords;
import com.syl.generator.common.util.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 基础javabean字段类
 *
 * @author syl
 * @create 2018-03-26 20:40
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class Filed {
    public static String ESCAPES = ConfigReadUtil.getYmlString("filed.escapes","`");
    private static boolean MATCH_KEYWORD = ConfigReadUtil.getYmlBoolean("generate.matchkeyword",true);
    private String type;
    private String originalName;//原始名称
    private String escapesName;//转义后名称
    private String camelName;//驼峰名
    private String backCamelName;//首字母不大写驼峰
    private boolean primaryKey;
    private String comment;
    private String interval;//间距
    private boolean acquired;
    private List<String> annotations = new ArrayList<>();
    /**
     * 新增属性符号
     */
    private String  symbol;

    public Filed(String type, String originalName,boolean useOriginalName) {
        this.type = type;
        this.originalName = originalName;
        if(MATCH_KEYWORD)setEscapesName(this.originalName);
        this.camelName = StringUtils.underlineToCamel(originalName,true);
        this.backCamelName = useOriginalName ? originalName : camelName.substring(0,1).toLowerCase()+camelName.substring(1);
    }

    public Filed setEscapesName(String originalName) {
        this.escapesName = SqlReservedWords.containsWord(originalName) ? ESCAPES+originalName+ESCAPES : originalName;
        return this;
    }

    public Filed addAnnotation(String annotation) {
        this.annotations.add(annotation);
        return this;
    }

    public Filed setInterval(int length){
        StringBuffer sb = new StringBuffer(" ");
        for (int i = 0; i < length; i++) {
            sb.append(" ");
        }
        this.interval = sb.toString();
        return this;
    }

}
