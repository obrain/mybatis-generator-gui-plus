package com.syl.generator.common.api.bean;

import com.syl.generator.common.bean.SystemConfigBean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author syl
 * @create 2018-04-07 16:52
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class ServiceBuild  extends JavaBuild {
    private static final String SERVICE = "Service";

    private String beanName;
    private String dtoName;

    public ServiceBuild(String templateDirectory, String templateFileName, String path, String fileName, String reference, SystemConfigBean systemConfigBean) {
        super(templateDirectory, templateFileName, path, fileName, reference, systemConfigBean);
    }

}
