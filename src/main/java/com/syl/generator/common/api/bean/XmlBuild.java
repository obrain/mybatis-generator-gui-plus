package com.syl.generator.common.api.bean;

import com.syl.generator.common.enums.FileType;

/**
 * @author syl
 * @create 2018-04-05 20:12
 **/
public class XmlBuild extends BaseBuild{

    public XmlBuild(String templateDirectory, String templateFileName, String path, String fileName, FileType fileType) {
        super(templateDirectory, templateFileName, path, fileName, fileType);
    }
}
