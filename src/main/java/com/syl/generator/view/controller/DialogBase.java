package com.syl.generator.view.controller;

import javafx.scene.Parent;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;

/**
 * 对话框基础类
 */
@SuppressWarnings("all")
public class DialogBase {
    @Getter
    @Setter
    private Stage dialogStage;
    @Getter
    @Setter
    private Parent parent;

    public void showDialogStage() {
         dialogStage.show();
    }

    public void closeDialogStage() {
        dialogStage.close();
    }

}
