package com.syl.generator.view.controller;

import com.syl.generator.common.bean.DatabaseBean;
import com.syl.generator.common.bean.TableColumnBean;
import com.syl.generator.common.dao.SqlLiteMapper;
import com.syl.generator.common.util.DbConnectionUtil;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Setter;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * 定制列控制器
 *
 * @author syl
 * @create 2018-04-07 0:31
 **/
public class SelectTableColumnController extends DialogBase  implements Initializable {
    private static final Logger LOG = Logger.getLogger(SelectTableColumnController.class);
    @Setter
    private MainUIController mainUIController;
    @FXML
    private TableView<TableColumnBean> columnListView;
    @FXML
    private TableColumn<TableColumnBean, Boolean> checkedColumn;
    @FXML
    private TableColumn<TableColumnBean, String> columnNameColumn;
    @FXML
    private TableColumn<TableColumnBean, String> jdbcTypeColumn;
    @FXML
    private TableColumn<TableColumnBean, String> javaTypeColumn;
    @FXML
    private TableColumn<TableColumnBean, String> propertyNameColumn;
    @FXML
    private TableColumn<TableColumnBean, String> typeHandlerColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LOG.info("open select table");
        checkedColumn.setCellValueFactory(new PropertyValueFactory<>("configID"));
    }

    public void ok(ActionEvent actionEvent) {

    }

    public void cancel(ActionEvent actionEvent) {
        closeDialogStage();
    }

    public SelectTableColumnController initColumnListView() {
        String id = mainUIController.getHideID().getText();
        String tableName = mainUIController.getTableNameField().getText();
        SqlLiteMapper dao = DbConnectionUtil.getSqliteDao();
        DatabaseBean databaseBean = dao.selectOneDbConnection(id);
        databaseBean.setTableName(tableName);
        List<TableColumnBean> list = DbConnectionUtil.getTableColumnList(databaseBean, 1);
        this.columnListView.setItems(FXCollections.observableList(list));
        return this;
    }
}
