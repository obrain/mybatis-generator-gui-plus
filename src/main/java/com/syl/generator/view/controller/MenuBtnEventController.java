package com.syl.generator.view.controller;

import com.syl.generator.common.constant.BaseConstant;
import com.syl.generator.view.enums.FXMLPage;
import com.syl.generator.view.util.UiBaseUtils;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;

/**
 * 菜单按钮事件控制器
 *
 * @author syl
 * @create 2018-03-17 12:55
 **/
public class MenuBtnEventController{
    private Label systemConfig;//系统设置按钮
    private Label connectionLabel;//打开连接
    private Label configsLabel;//保存配置
    private MainUIController main;

    public MenuBtnEventController(MainUIController main,Label connectionLabel, Label systemConfig, Label configsLabel) {
        this.systemConfig = systemConfig;
        this.connectionLabel = connectionLabel;
        this.configsLabel = configsLabel;
        this.main = main;
    }

    public void init(){
        UiBaseUtils.setMenuButtonIcon(systemConfig, BaseConstant.SYSTEM_ICON);
        UiBaseUtils.setMenuButtonIcon(connectionLabel,BaseConstant.COMPUTER_ICON);
        UiBaseUtils.setMenuButtonIcon(configsLabel,BaseConstant.CONFIG_ICON);
        startListening();
    }

    public void startListening(){
        systemConfig.setOnMouseClicked(event -> {
            MouseButton button = event.getButton();
            if(button != MouseButton.PRIMARY)return;
            UiBaseUtils.openDialog(SystemConfigController.class,systemConfig.getText(), FXMLPage.SYSTEM_CONFIG, true);

        });

        connectionLabel.setOnMouseClicked(event -> {
            MouseButton button = event.getButton();
            if(button != MouseButton.PRIMARY)return;
            DbConnectionController controller = (DbConnectionController)UiBaseUtils.openDialog(DbConnectionController.class,connectionLabel.getText(), FXMLPage.NEW_CONNECTION, true);
            controller.setMainUIController(main);
        });

        configsLabel.setOnMouseClicked(event -> {
            MouseButton button = event.getButton();
            if(button != MouseButton.PRIMARY)return;
            GeneratorConfigController controller = (GeneratorConfigController)UiBaseUtils.openDialog(GeneratorConfigController.class,configsLabel.getText(),FXMLPage.GENERATOR_CONFIG,true);
            controller.setMainUIController(main);
        });
    }
}
