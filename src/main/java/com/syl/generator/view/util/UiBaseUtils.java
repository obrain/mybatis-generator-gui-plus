package com.syl.generator.view.util;

import com.syl.generator.MainUI;
import com.syl.generator.common.bean.InputVerifyBean;
import com.syl.generator.view.enums.FXMLPage;
import com.syl.generator.common.util.StringUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextInputControl;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

/**
 * view 基础工具类
 *
 * @author syl
 * @create 2018-03-17 11:56
 **/
public class UiBaseUtils {
    private static final Logger LOG = Logger.getLogger(UiBaseUtils.class);

    /**
     * 设置菜单按钮图标
     * @param labeled
     * @param iconImg 图标路径
     */
    public static void setMenuButtonIcon(Labeled labeled, String iconImg){
        ImageView imageView = new ImageView(iconImg);
        imageView.setFitHeight(40);
        imageView.setFitWidth(50);
        labeled.setGraphic(imageView);
    }

    /**
     *
     * @param cz  xml对应的控制器
     * @param title  对话框标题
     * @param fxmlPage 对话框对应的xml文件
     * @param closed  是否可关闭
     * @return 控制器实例
     */
    public static Object openDialog(Class cz, String title, FXMLPage fxmlPage, boolean closed) {
        URL skeletonResource = Thread.currentThread().getContextClassLoader().getResource(fxmlPage.getFxml());
        FXMLLoader loader = new FXMLLoader(skeletonResource);
        Parent root;
        try {
            root = loader.load();
            Object o = loader.getController();
            Stage dialogStage = new Stage();
            dialogStage.setTitle(title);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(MainUI.mainStage);
            dialogStage.setScene(new Scene(root));
            dialogStage.setMaximized(false);
            dialogStage.setResizable(false);
            dialogStage.setOnCloseRequest(new MyWindowsCloseEvent(closed));

            dialogStage.show();
            PropertyDescriptor pd1 = new PropertyDescriptor("dialogStage",cz);
            PropertyDescriptor pd2 = new PropertyDescriptor("parent",cz);

            pd1.getWriteMethod().invoke(o,new Object[]{dialogStage});
            pd2.getWriteMethod().invoke(o,new Object[]{root});
            return o;
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 文本框验证是否为空方法
     * @param inputs
     * @param parent
     * @return
     */
    public static InputVerifyBean inputIsEmpty(TextInputControl[] inputs, Parent parent) {
        for (TextInputControl input : inputs) {
            String s = input.getText();
            String id = input.getId();
            Label label = (Label) parent.lookup("#" + id + "Label");
            if(StringUtils.isEmpty(s))return new InputVerifyBean(false,label.getText());
        }
        return new InputVerifyBean(true,"");
    }
}
