package com.syl.generator.view.util;

import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

/**
 * 自定义窗口关闭事件
 * @author syl
 * @create 2018-03-29 22:22
 **/
public class MyWindowsCloseEvent implements EventHandler<WindowEvent> {
    private boolean closed;

    public MyWindowsCloseEvent(boolean closed) {
        this.closed = closed;
    }

    @Override
    public void handle(WindowEvent event) {
        if(!closed)event.consume();//禁止窗口关闭
    }
}
