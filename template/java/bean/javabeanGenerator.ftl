package ${referencePackage};

<#list imports as import>
import ${import};
</#list>

/**
 * ${remark}
 *
 * @author ${author}
 * @create ${date}
 * ${brand}
 **/
<#list annotations as anno>
${anno}
</#list>
public class ${fileName} ${jextends} ${jimplements} {
    public ${fileName}(){}

<#list fileds as filed>
    /**
     * ${filed.comment}
     */
    <#list filed.annotations as annotation>
        ${annotation}
    </#list>
    private ${filed.type}${filed.interval}${filed.backCamelName};
</#list>
<#if !lombok>
<#list fileds as filed>

    /**
     * ${filed.comment} get
     */
    public ${filed.type} get${filed.camelName}() {
        return ${filed.backCamelName};
    }

    /**
     * ${filed.comment} set
     */
    public ${fileName} set${filed.camelName}(${filed.type} ${filed.backCamelName}) {
        this.${filed.backCamelName} = ${filed.backCamelName};
        return this;
    }
</#list>

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("${fileName}{");
        <#list fileds as filed>
        sb.append("${filed.backCamelName}=").append(${filed.backCamelName});
        </#list>
        sb.append("}");
        return sb.toString();
    }
</#if>

}
