package ${referencePackage};

<#list imports as import>
import ${import};
</#list>

/**
 * ${remark}
 *
 * @author ${author}
 * @create ${date}
 * ${brand}
 **/
<#list annotations as anno>
${anno}
</#list>
public interface ${daoName} {

    /**
     *  选择性插入一条记录
     */
    int insertSelective(${beanName} bean);

    <#-- 依据主键个数生成 start -->
    <#list fileds as filed>
    /**
     *  根据${filed.originalName}主键删除记录
     *  @param ${filed.backCamelName} ${filed.backCamelName}
     */
    int deleteBy${filed.camelName}(Serializable ${filed.backCamelName});

    /**
     *  根据${filed.originalName}主键批量删除数据
     *  @param idList ${filed.backCamelName}列表
     */
    int batchDeleteBy${filed.camelName}(@Param("idList") List<Serializable> idList);

    /**
     *  根据${filed.originalName}主键更新记录
     *  @param bean ${beanName}
     */
    int updateBy${filed.camelName}(@Param("bean") ${beanName} bean);

    /**
     *  根据${filed.originalName}主键批量更新数据
     *  仅限更新值为一样的情况
     *  @param bean ${beanName}
     *  @param idList ${filed.backCamelName}列表
     */
    int batchUpdateBy${filed.camelName}(@Param("bean") ${beanName} bean, @Param("idList") List<Serializable> idList);

    /**
     *  根据${filed.originalName}主键查询记录
     *  @param id ${filed.backCamelName}
     */
    ${beanName} selectBy${filed.camelName}(Serializable id);

    </#list>
    <#-- 依据主键个数生成 end -->
    /**
     *  查询所有记录
     */
    List<${beanName}> selectAll();

    /**
     *  依据条件查询一条记录
     */
    ${beanName} selectOne(@Param("condition") ${queryHelpName} condition, @Param("bean") ${beanNameDTO} bean);

    /**
     *  依据条件查询是否存在记录
     */
    boolean isExist(@Param("condition") ${queryHelpName} condition, @Param("bean") ${beanNameDTO} bean);

    /**
     *  依据条件查询记录列表
     */
    List<${beanName}> selectCondition(@Param("condition") ${queryHelpName} condition, @Param("bean") ${beanNameDTO} bean);

    /**
     *  依据条件查询记录数量
     */
    long selectConditionCount(@Param("condition") ${queryHelpName} condition, @Param("bean") ${beanNameDTO} bean);

}
